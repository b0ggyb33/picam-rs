use opencv::core;
use opencv::features2d;
use opencv::imgcodecs::imwrite;
use opencv::prelude::*;
use opencv::videoio;

fn main() {
    let mut cam = videoio::VideoCapture::new_default(0).expect("could not make cam");
    // 0 is the default camera
    let opened = videoio::VideoCapture::is_opened(&cam).expect("could not work out if open");
    if !opened {
        panic!("Unable to open default camera!");
    }

    let prop = cam
        .get(videoio::CAP_PROP_FORMAT)
        .expect("Could not get property");
    println!("Capture format: {}", prop);

    let mut frame = core::Mat::default().expect("could not make mat");
    let params: core::Vector<i32> = core::Vector::new();

    let mut detector = features2d::ORB::default().expect("Could not make detector");
    let mask = core::Mat::default().expect("could not make mat");
    let flags = features2d::DrawMatchesFlags_DEFAULT;

    println!("camera ready");
    loop {
        let filename = format!("frame.jpg");
        println!("{}", filename);
        cam.read(&mut frame).expect("could not read frame");

        let mut keypoints = core::Vector::<core::KeyPoint>::new();
        detector
            .detect(&frame, &mut keypoints, &mask)
            .expect("could not detect keypoints");

        println!("{} keypoints detected", keypoints.len());

        let mut out = core::Mat::default().expect("could not make mat");
        features2d::draw_keypoints(
            &frame,
            &keypoints,
            &mut out,
            core::Scalar::all(-1f64),
            flags,
        )
        .expect("could not draw keypoints");

        imwrite(&filename, &out, &params).expect("could not write");
    }
}
