FROM rustembedded/cross:arm-unknown-linux-gnueabihf-0.2.1

RUN dpkg --add-architecture armhf
RUN apt-get update && apt-get upgrade -yq
RUN apt-get install llvm --no-install-recommends -yq
RUN apt-get install clang libclang-dev --no-install-recommends -yq
RUN apt-get install crossbuild-essential-armhf -yq
RUN apt-get install cmake libavcodec-dev:armhf libavformat-dev:armhf libswscale-dev:armhf --no-install-recommends -yq
RUN git clone https://github.com/opencv/opencv --depth 1 /opt/opencv
WORKDIR /opt/opencv
RUN mkdir -p build
WORKDIR build
RUN cmake -D CMAKE_C_COMPILER=/usr/bin/arm-linux-gnueabihf-gcc-7 -DCMAKE_CXX_COMPILER=/usr/bin/arm-linux-gnueabihf-g++-7 -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
RUN make -j8
RUN make install
